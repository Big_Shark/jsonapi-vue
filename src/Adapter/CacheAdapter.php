<?php

namespace Adapter;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Class CacheAdapter
 * @package Adapter
 */
class CacheAdapter
{
    /**
     * @var CacheItemPoolInterface
     */
    protected $cache;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var string
     */
    protected $key;

    /**
     * CacheAdapter constructor.
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(CacheItemPoolInterface $cache, $key)
    {
        $this->cache = $cache;
        $this->key = $key;
        $this->load();
    }

    /**
     * @return array
     */
    public function find($criteria)
    {
        return array_filter($this->data, function($item) use ($criteria)
        {
            foreach($criteria as $key=>$value) {
                if(array_key_exists($key, $item) && $item[$key] != $value) {
                    return false;
                }
            }
            return true;
        });
    }

    /**
     * @param $data
     */
    public function store($data)
    {
        //TODO We need EntityManager
        foreach($this->data as $key=>$value) {
            if($data['id'] == $value['id']) {
                $this->data[$key] = $data;
            }
        }
        $this->save();
    }

    /**
     * @param $data
     */
    public function add($data)
    {
        $this->data[] = $data;
        $this->save();
    }

    /**
     * @param $data
     */
    public function remove($data)
    {
        //TODO We need EntityManager
        foreach($this->data as $key=>$value) {
            if($data['id'] == $value['id']) {
                unset($this->data[$key]);
            }
        }
        $this->save();
    }

    /**
     * @return void
     */
    protected function load()
    {
        $cacheItem = $this->getCacheItem();
        if (is_array($cacheItem->get())) {
            $this->data = $cacheItem->get();
        }
    }

    /**
     * @return void
     */
    public function save()
    {
        $cacheItem = $this->getCacheItem();
        $cacheItem->set($this->data);
        $this->cache->save($cacheItem);
    }

    /**
     * @return CacheItemInterface
     */
    protected function getCacheItem() : CacheItemInterface
    {
        return $this->cache->getItem($this->key);
    }
}