<?php

namespace Entity;

/**
 * Class Post
 */
class Post
{
    /**
     * @param string $title
     * @param string $content
     * @return Post
     */
    public static function new(string $title, string $content) : Post
    {
        return new Post(uniqid(), $title, $content);
    }

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $content;

    /**
     * Post constructor.
     * @param string $id
     * @param string $title
     * @param string $content
     */
    public function __construct(string $id, string $title, string $content)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->content;
    }

    /**
     * @param string $title
     * @param string $content
     * @return Post
     */
    public function update(string $title, string $content) : Post
    {
        $this->title = $title;
        $this->content = $content;
        return $this;
    }

}