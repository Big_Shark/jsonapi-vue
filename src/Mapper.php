<?php

class Mapper
{
    protected $adapter;

    public function __construct($adapter)
    {
        $this->adapter = $adapter;
    }

    public function collectionTo($array)
    {
        $data= [];
        foreach($array as $item) {
            $data[] = $this->to($item);
        }
        return $data;
    }

    public function collectionFrom($array)
    {
        $data= [];
        foreach($array as $item) {
            $data[] = $this->from($item);
        }
        return $data;
    }

    public function store($entity)
    {
        $this->adapter->store($this->from($entity));
    }

    public function find($criteria)
    {
        return $this->collectionTo($this->adapter->find($criteria));
    }

    public function add($entity)
    {
        $this->adapter->add($this->from($entity));
    }

    public function remove($entity)
    {
        $this->adapter->remove($this->from($entity));
    }
}