<?php

namespace Mapper;

use Entity\Post;

class PostMapper extends \Mapper
{
    public function to($data)
    {
        return new Post($data['id'], $data['title'], $data['content']);
    }

    public function from(Post $entity)
    {
        return ['id' => $entity->getId(), 'title' => $entity->getTitle(), 'content' => $entity->getContent()];
    }
}