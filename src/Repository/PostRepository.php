<?php

namespace Repository;
use Entity\Post;
use Mapper\PostMapper;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Class PostRepository
 */
class PostRepository
{
    /**
     * @var Post[]
     */
    protected $data = [];

    /**
     * @var PostMapper
     */
    protected $mapper;

    /**
     * PostRepository constructor.
     * @param PostMapper $mapper
     */
    public function __construct(PostMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param string $id
     * @return Post
     * @throws \Exception
     */
    public function find($id)
    {
        $entity = $this->mapper->find(['id' => $id]);
        if (isset($entity[0])) {
            return $entity[0];
        }

        throw new \Exception('Post not found'); //TODO Add custome Exception
    }


    /**
     * @return Post[]
     */
    public function findAll() : array
    {
        return $this->mapper->find([]);
    }

    /**
     * @return void
     */
    public function remove($id)
    {
        $this->mapper->remove($this->find($id));
    }

    /**
     * @param Post $post
     */
    public function add(Post $post)
    {
        $this->mapper->add($post);
    }

    /**
     * @param Post $post
     */
    public function store(Post $post)
    {
        $this->mapper->store($post);
    }

}