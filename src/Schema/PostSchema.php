<?php

namespace Schema;

/**
 * Class PostSchema
 */
class PostSchema extends \Neomerx\JsonApi\Schema\SchemaProvider
{
    /**
     * @var string
     */
    protected $resourceType = 'posts';

    /**
     * @param Post $post
     * @return string
     */
    public function getId($post)
    {
        return $post->getId();
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function getAttributes($post) : array
    {
        return [
            'title' => $post->getTitle(),
            'content'  => $post->getContent(),
        ];
    }
}