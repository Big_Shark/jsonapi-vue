<?php
// Routes

$app->get('/', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {

    return $this->renderer->render($response, 'index.phtml', ['debug' => $this->get('settings')['debug']]);
});

$app->get('/posts', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {

    $posts = $this->postRepository->findAll();

    $result = $this->get('encoder')->encodeData($posts);

    $response->getBody()->write($result);

    return $response;
});

$app->get('/posts/{id}', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {

    $post = $this->postRepository->find($args['id']);

    $result = $this->get('encoder')->encodeData($post);

    $response->getBody()->write($result);

    return $response;
});

$app->delete('/posts/{id}', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {

    $this->postRepository->remove($args['id']);

    return $response;
});

$app->post('/posts', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {

    $body = $request->getParsedBody();
    if (!$body || !isset($body['data'])) {
        $this->get('encode')->encodeError(new Error('Whoops'));
    }

    $attributes = $body['data']['attributes'];
    $post = Entity\Post::new($attributes['title'], $attributes['content']);
    $this->postRepository->add($post);

    $result = $this->get('encoder')->encodeData($post);

    $response->getBody()->write($result);
    return $response;
});

$app->patch('/posts/{id}', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {

    $body = $request->getParsedBody();

    if (!$body || !isset($body['data'])) {
        $this->get('encode')->encodeError(new Error('Whoops'));
    }

    $post = $this->postRepository->find($body['data']['id']);

    $attributes = $body['data']['attributes'];
    $post->update($attributes['title'], $attributes['content']);

    $this->postRepository->store($post);

    $result = $this->get('encoder')->encodeData($post);

    $response->getBody()->write($result);
    return $response;
});