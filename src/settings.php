<?php
return [
    'settings' => [
        'debug' => true,

        'url' => 'http://127.0.0.1:8000',

        'displayErrorDetails' => true, // set to false in production

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Memcached settings
        'memcached' => [
            'host' => 'localhost',
            'port' => 11211,
        ],
    ],
];
